#include <string>
#include <random>

using namespace std;

string randDNA(int seed, string bases, int n)
{
	mt19947 engine(seed);  //produces same output everytime
	int min =1; // min number to generate
	int max= bases.n(); //max number to generate
	string hold; //strimg that holds the output

	uniform_int_distribution<> unifrm(min, max); 

	for (int i = 1; i <= n; i++)
	{
		switch(unifrm(engine))
		{
			case 1: hold += bases[0];
			break;
			case 2: hold += bases[1];
			break;
			case 3: hold += bases[2];
			break;
			case 4: hold += bases[3];
			break;
			case 5: hold += bases[4];
			break;
			case 6: hold += bases[5];
			break;
			case 7: hold += bases[6];
			break;
		}
	}

return hold;


}
